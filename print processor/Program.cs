﻿using System;
using System.IO;

namespace print_processor
{
    class Program
    {
        static void Main(string[] args)
        {
            // When used with the TROY Secure Print PCL6 v4 driver, args[0] will contain "-p"
            // and args[1] will contain the name of the print queue that is printing. If you need
            // to know the name of the print queue that sent the job, you can grab it from args[1].
            var printQueueOption = args.Length > 0 ? args[0] : "";
            var printQueueName = args.Length > 1 && printQueueOption == "-p" ? args[1] : "";
            
            // Console.WriteLine($"Option: {printQueueOption}");
            // Console.WriteLine($"Value: {printQueueName}");
            
            // read the incoming PCL6 stream
            var pclStreamIn = ReadStream();

            // manipulate the PCL as necessary
            var pclStreamOut = ManipulateStream(pclStreamIn);

            // write the new PCL6 stream back out
            WriteStream(pclStreamOut);
        }

        /// <summary>
        /// Reads the standard input stream into memory. This acquires the PCL6 stream from the print pipeline filter
        /// process associated with the driver.
        /// </summary>
        /// <returns>A <see cref="MemoryStream"/> that contains the PCL6 from the driver.</returns>
        static MemoryStream ReadStream()
        {
            var stream = new MemoryStream();
            using (var inputStream = Console.OpenStandardInput())
            {
                inputStream.CopyTo(stream);
            }
            return stream;
        }

        /// <summary>
        /// Use this method to make changes to the incoming PCL6.
        /// </summary>
        /// <param name="stream">The original PCL6 stream from the driver.</param>
        /// <returns>A <see cref="MemoryStream"/> that contains the new PCL6 stream.</returns>
        static MemoryStream ManipulateStream(MemoryStream stream)
        {
            // TODO manipulate the stream as necessary and then return it
            return stream;
        }

        /// <summary>
        /// Writes the memory stream to standard output. This sends the manipulated PCL6 back to the print
        /// pipeline filter process.
        /// </summary>
        /// <param name="stream"></param>
        static void WriteStream(MemoryStream stream)
        {
            stream.Position = 0;
            using var outputStream = Console.OpenStandardOutput();
            stream.CopyTo(outputStream);
        }
    }
}
