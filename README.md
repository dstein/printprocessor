# TROY Secure Print PCL6 v4 Driver and Print Processor

The TROY Secure Print PCL6 v4 driver can be used for any printer that accepts PCL6. It uses Microsoft's XPS to PCL6 pipeline filter to generate PCL6. This PCL6 stream can be passed to an executable which can manipulate the stream and send it back to the driver to be passed along to the printer.

## Quickstart

Here's an overview of the steps for testing this new workflow. The rest of this document has more detail about how to complete each step.

1. Build a print processor executable to handle print jobs from the driver (sample code is in the Print Processor section).
2. Install the new TROY Secure Print PCL6 v4 driver (see the driver section).
3. Configure a printer to use the new driver and to launch your print processor executable when jobs are received (see the driver section).
4. Send test jobs to the new printer.

## Print Processor

I created a Visual Studio 2019 solution with a print processor project to demonstrate this workflow. This section describes the project as well as how/where you can update it for your purposes.

### Source Code

The source code for the Print Processor solution is in Bitbucket. Feel free to pull it down and use it however you like. It's just starter code that demonstrates how to interact with the driver.

<https://bitbucket.org/dstein/printprocessor/src/master/>

The solution has a single project (Print Processor) that is a .NET Core Console application.  It's configured as a self-contained single executable (I included a link below for reference if you're interested in how this kind of executable works, feel free to ignore it). 

<https://www.hanselman.com/blog/MakingATinyNETCore30EntirelySelfcontainedSingleExecutable.aspx>

### Executable

The output from the Print Processor project is an executable that's intended to be used with the TROY Secure Print PCL6 v4 driver (there's a section further down in this file that explains how to configure the driver to use the executable).

The executable is invoked by the driver with the command below. The driver does this automatically. I'm just including the command here so you know what it's doing under the covers.

_printprocessor.exe -p "Print Queue 1"_

    The -p option provides the name of the print queue that sent the print job.

In addition to the queue name, the driver also sends the PCL6 stream to the executable's standard input.

### Current Version
The current version of the print processor simply reads the incoming PCL6 stream and then sends it right back to the driver, unaltered. The _Program.cs_ file has comments throughout to explain what's going on and where you can insert code to manipulate the stream as needed before sending it back to the driver.

### Creating the Executable

Right-click the  Print Processor project and select Publish.  From the Publish dialog, choose the option to publish to a folder. When publishing is finished, that folder will have the printprocessor.exe file which can be copied to a computer for use with the driver.

## TROY Secure Print PCL6 v4 Driver

I'll send a link to the current driver files for testing.

_This hasn't gone through any real testing and shouldn't be used in production at this time._

### Installation

Download and unzip the driver files from above to a folder on your computer and then follow these steps:

1. Open Windows Print Management.
2. Select Action > Add Driver.
3. Click Next on the Welcome screen.
4. Select x64 on the Processor Selection screen and click Next.
5. Select Have Disk on the Driver Selection screen.
6. Browse to the folder where you unzipped the driver files, select the TroySecurePrintDriver.inf file, and click Open.
7. Click Ok, Next, and then Finish on the next 3 dialogs to finish the installation.

### Registry Keys

The driver uses registry keys to locate the executable that it should launch when printing. One of the files you unzipped earlier has the necessary keys to be imported into the registry (_Troy Secure Print Driver.reg_). Follow these steps to add the keys to the registry:

1. Import the registry keys by double-clicking the .reg file.
3. Grant the Users group Full Control to the new TROY Group key in the registry. This allows our custom printer extension to make changes to the file path (our custom extension is displayed when you open the Print Preferences dialog for a printer that uses our driver).

_The keys in the .reg file specify: a log file location (LogFilePath), an output file location (OutputFilePath), and an executable file path (ExecFilePath). All three keys are optional, but either an output file path or executable file path is needed for the driver to actually do something._

### Printer Configuration

After installing the driver and importing the registry keys, add a new printer using the driver (or update an existing printer to use it).

_Be sure the printer is configured as a TCP/IP printer (NOT a Web Service printer)._

From the Printer Preferences dialog in the Printer Properties, you can edit the executable file path that is launched when a print job is received. Doing so updates the ExecFilePath registry location.

